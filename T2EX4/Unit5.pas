unit Unit5;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, Unit4;

type
  TForm5 = class(TForm)
    DBLookupComboBox1: TDBLookupComboBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Btn_inserir: TButton;
    Btn_cancelar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure Btn_cancelarClick(Sender: TObject);
    procedure Btn_inserirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

{$R *.dfm}

procedure TForm5.Btn_cancelarClick(Sender: TObject);
begin
Close;
end;

procedure TForm5.Btn_inserirClick(Sender: TObject);
begin
DataModule4.FDQueryPokemon.Append;
end;

end.
