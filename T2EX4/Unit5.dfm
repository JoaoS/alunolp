object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'Form5'
  ClientHeight = 329
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 81
    Top = 147
    Width = 27
    Height = 13
    Caption = 'Nivel'
  end
  object Label2: TLabel
    Left = 81
    Top = 96
    Width = 57
    Height = 13
    Caption = 'Treinador'
  end
  object Label3: TLabel
    Left = 81
    Top = 48
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 144
    Top = 96
    Width = 145
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule4.DataSourcePokemon
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule4.DataSourceTreinador
    TabOrder = 0
  end
  object DBEdit1: TDBEdit
    Left = 144
    Top = 45
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule4.DataSourcePokemon
    TabOrder = 1
  end
  object DBEdit2: TDBEdit
    Left = 144
    Top = 144
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule4.DataSourcePokemon
    TabOrder = 2
  end
  object Btn_inserir: TButton
    Left = 230
    Top = 198
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 3
    OnClick = Btn_inserirClick
  end
  object Btn_cancelar: TButton
    Left = 144
    Top = 198
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 4
    OnClick = Btn_cancelarClick
  end
end
