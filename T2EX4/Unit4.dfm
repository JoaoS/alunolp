object DataModule4: TDataModule4
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 293
  Width = 464
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=pokemon'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 144
    Top = 16
  end
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 40
    Top = 16
  end
  object FDQueryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from pokemon')
    Left = 216
    Top = 16
  end
  object DataSourcePokemon: TDataSource
    DataSet = FDQueryPokemon
    Left = 304
    Top = 16
  end
  object FDQueryTreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from treinador')
    Left = 224
    Top = 72
  end
  object DataSourceTreinador: TDataSource
    DataSet = FDQueryTreinador
    Left = 304
    Top = 72
  end
end
