unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit4, Vcl.DBCtrls, Vcl.StdCtrls,
  Vcl.ExtCtrls, Unit5;

type
  TForm3 = class(TForm)
    Panel1: TPanel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    Lb_identificador: TLabel;
    Lb_treinador: TLabel;
    Lb_nome: TLabel;
    Lb_nivel: TLabel;
    Btn_editar: TButton;
    Btn_inserir: TButton;
    Btn_deletar: TButton;
    DBLookupListBox1: TDBLookupListBox;
    procedure Btn_editarClick(Sender: TObject);
    procedure Btn_deletarClick(Sender: TObject);
    procedure Btn_inserirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;
implementation

{$R *.dfm}

procedure TForm3.Btn_deletarClick(Sender: TObject);
begin
DataModule4.FDQueryPokemon.Delete;
end;

procedure TForm3.Btn_editarClick(Sender: TObject);
begin
  Form5.ShowModal;
end;

procedure TForm3.Btn_inserirClick(Sender: TObject);
begin
Form5.ShowModal;
end;



end.
