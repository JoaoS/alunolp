object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 430
  ClientWidth = 566
  Color = clAqua
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 288
    Top = 0
    Width = 281
    Height = 241
    Color = clBtnHighlight
    ParentBackground = False
    TabOrder = 0
    object DBText1: TDBText
      Left = 112
      Top = 40
      Width = 65
      Height = 17
      DataField = 'id'
      DataSource = DataModule4.DataSourcePokemon
    end
    object DBText2: TDBText
      Left = 112
      Top = 80
      Width = 65
      Height = 17
      DataField = 'id_treinador'
      DataSource = DataModule4.DataSourcePokemon
    end
    object DBText3: TDBText
      Left = 112
      Top = 114
      Width = 65
      Height = 17
      DataField = 'nome'
      DataSource = DataModule4.DataSourcePokemon
    end
    object DBText4: TDBText
      Left = 112
      Top = 152
      Width = 65
      Height = 17
      DataField = 'nivel'
      DataSource = DataModule4.DataSourcePokemon
    end
    object Lb_identificador: TLabel
      Left = 40
      Top = 40
      Width = 59
      Height = 13
      Caption = 'identificador'
    end
    object Lb_treinador: TLabel
      Left = 40
      Top = 80
      Width = 46
      Height = 13
      Caption = 'Treinador'
    end
    object Lb_nome: TLabel
      Left = 40
      Top = 114
      Width = 27
      Height = 13
      Caption = 'Nome'
    end
    object Lb_nivel: TLabel
      Left = 40
      Top = 152
      Width = 23
      Height = 13
      Caption = 'Nivel'
    end
  end
  object Btn_editar: TButton
    Left = 328
    Top = 272
    Width = 137
    Height = 25
    Caption = 'Editar'
    TabOrder = 1
    OnClick = Btn_editarClick
  end
  object Btn_inserir: TButton
    Left = 328
    Top = 320
    Width = 137
    Height = 25
    Caption = 'Inserir Pokemon'
    TabOrder = 2
    OnClick = Btn_inserirClick
  end
  object Btn_deletar: TButton
    Left = 328
    Top = 368
    Width = 137
    Height = 25
    Caption = 'Deletar'
    TabOrder = 3
    OnClick = Btn_deletarClick
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 0
    Top = -3
    Width = 225
    Height = 433
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule4.DataSourcePokemon
    TabOrder = 4
  end
end
