object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 456
  ClientWidth = 767
  Color = clMedGray
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 64
    Top = 53
    Width = 33
    Height = 16
    Caption = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 64
    Top = 106
    Width = 37
    Height = 16
    Caption = 'Classe'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 64
    Top = 162
    Width = 27
    Height = 16
    Caption = 'N'#237'vel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 64
    Top = 218
    Width = 40
    Height = 16
    Caption = 'Ataque'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 65
    Top = 274
    Width = 39
    Height = 16
    Caption = 'Defesa'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 66
    Top = 325
    Width = 25
    Height = 16
    Caption = 'Vida'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object ed_nome: TEdit
    Left = 64
    Top = 75
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object ed_classe: TEdit
    Left = 64
    Top = 128
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object ed_nivel: TEdit
    Left = 64
    Top = 184
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object ed_ataque: TEdit
    Left = 64
    Top = 240
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object ed_defesa: TEdit
    Left = 64
    Top = 296
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object ed_vida: TEdit
    Left = 64
    Top = 347
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object bt_inserir: TButton
    Left = 264
    Top = 215
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 6
    OnClick = bt_inserirClick
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 65
    Top = 384
  end
end
