unit UnitPersonagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent;

type
  TForm1 = class(TForm)
    ed_nome: TEdit;
    ed_classe: TEdit;
    ed_nivel: TEdit;
    ed_ataque: TEdit;
    ed_defesa: TEdit;
    ed_vida: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    bt_inserir: TButton;
    NetHTTPClient1: TNetHTTPClient;
    procedure bt_inserirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.bt_inserirClick(Sender: TObject);
var
   nome, classe: string;
   nivel, ataque, defesa, vida: integer;

begin

     nome := ed_nome.Text;
     classe := ed_classe.Text;
     nivel := strtoint(ed_nivel.Text);
     ataque := strtoint(ed_ataque.Text);
     defesa := strtoint(ed_defesa.Text);
     vida := strtoint(ed_vida.Text);

     NetHTTPClient1.Get('https://venson.net.br/ws/personagens/?op=salvar&nome=' + nome + '&classe=' + classe + '&nivel=' + nivel.ToString +'&ata=' + ataque.ToString +'&def=' + defesa.ToString + '&life=' +vida.ToString);


end;

end.
