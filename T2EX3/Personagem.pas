unit Personagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.StdCtrls, Unit1;

type
  TForm3 = class(TForm)
    lista_personagens: TListBox;
    bt_atualizar: TButton;
    NetHTTPClient1: TNetHTTPClient;
    label1: TLabel;
    bt_inserir: TButton;
    procedure bt_atualizarClick(Sender: TObject);
    procedure bt_inserirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TPersonagem = class(TOBject)

    p_nome: string;
    p_classe: string;
    p_nivel: integer;
    p_ataque: integer;
    p_defesa: integer;
    p_vida: integer;

  end;

var
  Form3: TForm3;
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm3.bt_atualizarClick(Sender: TObject);
var
   Conteudo:string;
   ListaPersonagem: TArray<String>;
   Lista: string;
   Personagem: TPersonagem;
   Atributos: TArray<string>;

begin
   Conteudo := NetHTTPClient1.Get('https://venson.net.br/ws/personagens/').ContentAsString();
   ListaPersonagem := Conteudo.Split(['&']);

   for Lista in ListaPersonagem do
   begin
       Atributos := Lista.Split([';']);
       Personagem := TPersonagem.Create();

       Personagem.p_nome := Atributos[0];
       Personagem.p_classe := Atributos[1];
       Personagem.p_nivel := Atributos[2].ToInteger;
       Personagem.p_ataque := Atributos[3].ToInteger;
       Personagem.p_defesa := Atributos[4].ToInteger;
       Personagem.p_vida := Atributos[5].ToInteger;

       lista_personagens.AddItem(Personagem.p_nome,Personagem);
       lista_personagens.AddItem(Personagem.p_classe,Personagem);
       lista_personagens.AddItem(Personagem.p_nivel.ToString,Personagem);
       lista_personagens.AddItem(Personagem.p_ataque.ToString,Personagem);
       lista_personagens.AddItem(Personagem.p_defesa.ToString,Personagem);
       lista_personagens.AddItem(Personagem.p_vida.ToString,Personagem);
   end;


end;

procedure TForm3.bt_inserirClick(Sender: TObject);
begin
     Form1 := TForm1.Create(Application);
     Form1.Show;
end;

end.
