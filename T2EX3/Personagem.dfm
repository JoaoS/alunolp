object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 375
  ClientWidth = 628
  Color = clMedGray
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object label1: TLabel
    Left = 184
    Top = 32
    Width = 192
    Height = 25
    Caption = 'Lista de Personagens'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Yu Gothic'
    Font.Style = []
    ParentFont = False
  end
  object lista_personagens: TListBox
    Left = 200
    Top = 80
    Width = 281
    Height = 161
    ItemHeight = 13
    TabOrder = 0
  end
  object bt_atualizar: TButton
    Left = 88
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 1
    OnClick = bt_atualizarClick
  end
  object bt_inserir: TButton
    Left = 88
    Top = 159
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 2
    OnClick = bt_inserirClick
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 552
    Top = 224
  end
end
